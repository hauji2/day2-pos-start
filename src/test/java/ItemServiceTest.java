import org.example.dto.Item;
import org.example.dto.RawItemInput;
import org.example.service.ItemService;
import org.example.service.RawItemInputService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class ItemServiceTest {

    @Test
    void test_constructItems() {
        ItemService itemService = new ItemService();
        List<RawItemInput> rawItemInputServiceList = new RawItemInputService().constructRawDateInputs();
        List<Item> items = itemService.constructItems(rawItemInputServiceList);
        System.out.println(items);
        assertNotNull(items);
        assertNotEquals(items.size(), 0);
    }
}
