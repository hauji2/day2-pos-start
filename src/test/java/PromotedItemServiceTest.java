import org.example.dto.Item;
import org.example.dto.RawItemInput;
import org.example.service.ItemService;
import org.example.service.PromotedItemService;
import org.example.service.RawItemInputService;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

public class PromotedItemServiceTest {

    @Test
    void test_constructPromotedItems() {
        PromotedItemService promotedItemService = new PromotedItemService();
        List<RawItemInput> rawItemInputServiceList = new RawItemInputService().constructRawDateInputs();
        List<Item> items = new ItemService().constructItems(rawItemInputServiceList);
        List<Item> actual = promotedItemService.constructPromotedItems(items);
        System.out.println(actual);
        assertNotNull(actual);
        assertNotEquals(actual.size(), 0);
    }
}
