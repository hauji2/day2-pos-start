import org.example.dto.Item;
import org.example.dto.RawItemInput;
import org.example.service.ItemService;
import org.example.service.PromotedItemService;
import org.example.service.RawItemInputService;
import org.example.service.ReceiptService;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ReceiptServiceTest {

    @Test
    public void test_generateReceipt() {
        PromotedItemService promotedItemService = new PromotedItemService();
        List<RawItemInput> rawItemInputServiceList = new RawItemInputService().constructRawDateInputs();
        List<Item> items = new ItemService().constructItems(rawItemInputServiceList);
        List<Item> promotedItems = promotedItemService.constructPromotedItems(items);
        String receipt = new ReceiptService().generateReceipt(items, promotedItems);
        System.out.println(receipt);
    }
}
