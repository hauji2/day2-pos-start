import org.example.service.RawItemInputService;
import org.example.dto.RawItemInput;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.Assert.assertNotEquals;

public class RawItemInputServiceTest {
    RawItemInputService rawItemInputService = new RawItemInputService();


    @Test
    public void test_ConstructBarCodeList() {
        List<RawItemInput> barCodeList = rawItemInputService.constructRawDateInputs();
        System.out.println(barCodeList);
        assertNotEquals(barCodeList.size(), 0);

    }
}
