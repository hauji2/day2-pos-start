package org.example.service;

import org.example.PosDataLoader;
import org.example.dto.Item;
import org.example.dto.Quantity;


import java.util.Arrays;
import java.util.List;

public class PromotedItemService {

    public List<Item> constructPromotedItems(List<Item> items) {
        List<String> rawPromotedItems = Arrays.asList(PosDataLoader.loadPromotion());
        List<Item> promotedItemsInList = filterPromotedItems(rawPromotedItems, items);
        promotedItemsInList.forEach(this::buildPromotedItem);
        return promotedItemsInList.stream()
                .map(this::buildPromotedItem)
                .filter(item -> item.getPrice() > 0)
                .toList();    }

    private List<Item> filterPromotedItems(List<String> promotedItems, List<Item> items) {
        return promotedItems.stream().map(promotedItem -> items.stream().
                        filter(item -> item.getBarCode().equals(promotedItem)).
                        findFirst().
                        orElse(null))
                .toList();
    }

    private Item buildPromotedItem(Item originalItem) {
        Item item = Item.builder()
                .barCode(originalItem.getBarCode())
                .name(originalItem.getName())
                .itemUnit(originalItem.getItemUnit())
                .unitPrice(originalItem.getUnitPrice())
                .PriceUnit(originalItem.getPriceUnit())
                .quantity(Quantity.builder().build())
                .build();

        Integer freeItemCount = originalItem.getQuantity().getIntegerQuantity() / 3;
        Float price = item.getUnitPrice() * freeItemCount;
        item.getQuantity().setIntegerQuantity(freeItemCount);
        item.setPrice(price);
        return item;
    }

}
