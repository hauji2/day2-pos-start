package org.example.service;

import org.example.CommonUtils;
import org.example.PosDataLoader;
import org.example.dto.Item;
import org.example.dto.Quantity;
import org.example.dto.RawItemInput;

import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ItemService {

    CommonUtils commonUtils = new CommonUtils();

    public List<Item> constructItems(List<RawItemInput> rawItemInputs) {
        Map<String,String> rawItems = PosDataLoader.loadAllItems();
        return filterPurchasedItems(rawItemInputs, rawItems);
    }

    private List<Item> filterPurchasedItems(List<RawItemInput> rawItemInputs, Map<String, String> rawItems) {
        return rawItemInputs.stream().map(rawItemInput -> buildItem(rawItems, rawItemInput)).toList();
    }

    private Item buildItem(Map<String, String> rawItems, RawItemInput rawItemInput) {
        String barCode = rawItemInput.getBarCode();
        String connector = ",";
        String rawItem = rawItems.get(barCode);

        String[] split = rawItem.split(connector);

        Quantity quantity = rawItemInput.getQuantity();


        Float unitPrice = Float.parseFloat(split[1]);

        Item item= Item.builder()
                .barCode(barCode)
                .name(split[0])
                .itemUnit(split[2])
                .quantity(quantity)
                .PriceUnit("CNY")
                .unitPrice(unitPrice)
                .build();

        if (Objects.isNull(quantity.getIntegerQuantity())) {
            item.setPrice(unitPrice * quantity.getFloatQuantity());
        } else {
            item.setPrice(unitPrice * quantity.getIntegerQuantity());
        }

        return item;
    }
}
