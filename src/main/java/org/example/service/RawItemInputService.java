package org.example.service;

import org.example.CommonUtils;
import org.example.PosDataLoader;
import org.example.dto.Quantity;
import org.example.dto.RawItemInput;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class RawItemInputService {

    CommonUtils commonUtils = new CommonUtils();

    public List<RawItemInput> constructRawDateInputs(){
        List<String> rawBarCodeList = Arrays.stream(PosDataLoader.loadCart()).toList();
        return constructRawDateInputs(rawBarCodeList);
    }

    private List<RawItemInput> constructRawDateInputs(List<String> barCodes){
        return barCodes.stream().map(barCode -> constructRawDateInput(barCodes, barCode)).distinct().toList();
    }

    private RawItemInput constructRawDateInput(List<String> barCodes, String barCode) {
        String connector = "-";
        if (barCode.contains(connector)) {
            return buildRawItemInputWhenHaveConnector(barCode, connector);
        }

        return buildRawItemInputWhenNoConnector(barCodes, barCode);
    }

    private RawItemInput buildRawItemInputWhenHaveConnector(String barCode, String connector) {
        String[] split = barCode.split(connector);
        RawItemInput rawItemInput = RawItemInput.builder()
                .barCode(split[0])
                .quantity(Quantity.builder().build())
                .build();

        enrichQuantityForRawItemInput(split, rawItemInput);

        return rawItemInput;
    }

    private void enrichQuantityForRawItemInput(String[] split, RawItemInput rawItemInput) {
        if(commonUtils.isInteger(split[1])){
            rawItemInput.getQuantity().setIntegerQuantity(Integer.parseInt(split[1]));
            rawItemInput.getQuantity().setFloatQuantity(null);
        } else {
            rawItemInput.getQuantity().setFloatQuantity(Float.parseFloat(split[1]));
            rawItemInput.getQuantity().setIntegerQuantity(null);
        }
    }

    private RawItemInput buildRawItemInputWhenNoConnector(List<String> barCodes, String barCode) {
        Integer count = Collections.frequency(barCodes, barCode);
        return RawItemInput.builder()
                .barCode(barCode)
                .quantity(Quantity.builder().
                        integerQuantity(count).
                        floatQuantity(null).
                        build())
                .build();
    }
}
