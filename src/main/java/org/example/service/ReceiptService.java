package org.example.service;

import org.example.dto.Accounting;
import org.example.dto.Item;
import org.example.dto.Quantity;

import java.util.List;

public class ReceiptService {
    public String generateReceipt(List<Item> items,List<Item> promotedItems) {
        StringBuilder receipt = new StringBuilder();

        Accounting accounting = buildAccounting(items, promotedItems);

        receipt.append("***<No Profit Store> Shopping List***\n");
        receipt.append("----------------------\n");
        receipt.append(InsertItemsIntoReceipt(items));
        receipt.append("----------------------\n");
        receipt.append("Buy two get one free items:\n");
        receipt.append(InsertPromotedItemsIntoReceipt(promotedItems));
        receipt.append("----------------------\n");
        receipt.append(String.format("Total: %.2f(CNY)\n", accounting.getTotal() - accounting.getSaved()));
        receipt.append(String.format("Saved: %.2f(CNY)\n", accounting.getSaved()));
        receipt.append("**********************\n");
        return receipt.toString();
    }

    private String InsertItemsIntoReceipt(List<Item> items) {
        StringBuilder receiptItems = new StringBuilder();

        for (Item item : items) {
            String line;
            Quantity quantity = item.getQuantity();
            if (quantity.getIntegerQuantity() != null) {
                line = String.format("Name: %s, Quantity: %d %s%s, Unit Price: %.2f(CNY), Subtotal: %.2f(CNY)\n",item.getName(),item.getQuantity().getIntegerQuantity(),item.getItemUnit(),item.getQuantity().getIntegerQuantity()<=1 ? "" : "s",item.getUnitPrice(),item.getPrice());
            }
            else {
                line = String.format("Name: %s, Quantity: %.1f %s%s, Unit Price: %.2f(CNY), Subtotal: %.2f(CNY)\n",item.getName(),item.getQuantity().getFloatQuantity(),item.getItemUnit(),item.getQuantity().getFloatQuantity()<=1 ? "" : "s",item.getUnitPrice(),item.getPrice());
            }

            receiptItems.append(line);
        }

        return receiptItems.toString();
    }

    private String InsertPromotedItemsIntoReceipt(List<Item> promotedItems) {
        StringBuilder receiptItems = new StringBuilder();

        for (Item item : promotedItems) {
            String line;
            Quantity quantity = item.getQuantity();
            if (quantity.getIntegerQuantity() != null) {
                line = String.format("Name: %s, Quantity: %d %s%s, Value: %.2f(CNY)\n",item.getName(),item.getQuantity().getIntegerQuantity(),item.getItemUnit(),item.getQuantity().getIntegerQuantity()<=1 ? "" : "s",item.getPrice());
            }
            else {
                line = String.format("Name: %s, Quantity: %.1f %s&s, Value: %.2f(CNY)\n",item.getName(),item.getQuantity().getFloatQuantity(),item.getItemUnit(),item.getQuantity().getFloatQuantity()<=1 ? "" : "s",item.getPrice());
            }

            receiptItems.append(line);
        }

        return receiptItems.toString();
    }

    private Accounting buildAccounting(List<Item> items, List<Item> promotedItems) {
        Accounting accounting = new Accounting();
        accounting.setTotal((float) items.stream().mapToDouble(Item::getPrice).sum());
        accounting.setSaved((float) promotedItems.stream().mapToDouble(Item::getPrice).sum());
        return accounting;
    }
}
