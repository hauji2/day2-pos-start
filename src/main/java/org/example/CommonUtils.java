package org.example;

public class CommonUtils {
    public boolean isInteger(String number) {
        try {
            double doubleValue = Double.parseDouble(number);
            return doubleValue == Math.floor(doubleValue);
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
