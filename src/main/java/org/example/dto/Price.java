package org.example.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Price {
    private Float floatUnitPrice;
    private Integer integerUnitPrice;
}
