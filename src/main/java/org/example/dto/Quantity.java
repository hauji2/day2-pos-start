package org.example.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Quantity {

    private Integer integerQuantity;
    private Float floatQuantity;
}
