package org.example.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
public class Item {
    private String name;
    private String barCode;
    private Quantity quantity;
    private String itemUnit;
    private String PriceUnit;
    private Float unitPrice;
    private Float price;
}
