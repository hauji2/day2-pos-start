package org.example;


import org.example.dto.Item;
import org.example.dto.RawItemInput;
import org.example.service.ItemService;
import org.example.service.PromotedItemService;
import org.example.service.RawItemInputService;
import org.example.service.ReceiptService;

import java.util.List;

public class Printer {
    public String print(String[] cart, String[] promotions) {

        List<RawItemInput> barCodeList = new RawItemInputService().constructRawDateInputs();
        List<Item> items = new ItemService().constructItems(barCodeList);
        List<Item> promotedItems = new PromotedItemService().constructPromotedItems(items);
        return new ReceiptService().generateReceipt(items, promotedItems);
    }
}

